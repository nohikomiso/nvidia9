FROM nvidia/cuda:9.0-base-ubuntu16.04

LABEL maintainer "nohikomiso@gmail.com"

RUN apt-get update
RUN apt-get -y install wget curl automake libssl-dev libcurl4-openssl-dev
RUN wget https://www.dropbox.com/s/0d85ooxo9692ap4/miner_all9_3.tar.gz?dl=0 -O miner_all9.tar.gz
RUN tar xfvz miner_all9.tar.gz -C /root
WORKDIR /root
RUN echo '/root/miner_check.sh' > /root/onstart.sh
